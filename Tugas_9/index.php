<?php

require('animal.php');
require ('ape.php');
require ('frog.php');

$sheep = new Animal("shaun");

echo "Nama hewan: $sheep->name <br>"; // "shaun"
echo "Jumlah kaki: $sheep->legs <br>"; // 4
echo "Apakah termasuk hewan berdarah dingin? $sheep->cold_blooded <br><br>"; // "no"

$sungokong = new Ape("kera sakti");
echo "Nama hewan: $sungokong->name <br>";
echo "Jumlah kaki: $sungokong->legs <br>";
echo "Apakah termasuk hewan berdarah dingin? $sungokong->cold_blooded <br>"; // "no"
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "<br><br>Nama hewan: $kodok->name <br>";
echo "Jumlah kaki: $kodok->legs <br>";
echo "Apakah termasuk hewan berdarah dingin? $kodok->cold_blooded <br>"; // "no"
$kodok->jump();


 